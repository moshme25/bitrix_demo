<?php

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/vendor/autoload.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/vendor/autoload.php");
}

use Arrilot\BitrixBlade\BladeProvider;
use \Bitrix\Main\Data\Cache;

BladeProvider::register();

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler('', 'UserAdressOnAfterUpdate', 'OnAfterUpdate');

function OnAfterUpdate(\Bitrix\Main\Entity\Event $event) {
    CBitrixComponent::clearComponentCache('moshme25:user_adresslist');
    $cache = Cache::createInstance(); 
    $cache->clean('hl_adress_array1');
}

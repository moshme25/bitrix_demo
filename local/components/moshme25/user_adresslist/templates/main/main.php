<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\Grid\Options as GridOptions;
use Bitrix\Main\UI\PageNavigation;

if ($arResult['SHOW_TABLE']) {

    $list_id = 'example_list';

    $grid_options = new GridOptions($list_id);
    $sort = $grid_options->GetSorting([
        'sort' => ['ID' => 'DESC'],
        'vars' => [
            'by' => 'by',
            'order' => 'order'
            ]
        ]);
    $nav_params = $grid_options->GetNavParams();


    $nav = new PageNavigation('request_list');
    $nav->allowAllRecords(true)
        ->setPageSize($nav_params['nPageSize'])
        ->initFromUri();
    ?>

        <h2>Таблица</h2>

    <?php

    $columns = [];
    $columns[] = ['id' => 'ID', 'name' => 'ID', 'sort' => 'ID', 'default' => true];
    $columns[] = ['id' => 'ADRESS', 'name' => 'Адрес', 'sort' => 'ADRESS', 'default' => true];
    $columns[] = ['id' => 'ACTIVE', 'name' => 'Активность', 'sort' => 'ACTIVE', 'default' => true];
    foreach ($arResult['ITEMS'] as $row) {
        $list[] = [
            'data' => [
                "ID" => $row['ID'],
                "ADRESS" => $row['UF_ADRESS'],
                "ACTIVE" => $row['UF_ACTIVE'] == '1' ? 'Y': 'N',
            ],
        ];
    }

    $APPLICATION->IncludeComponent('bitrix:main.ui.grid', '', [
        'GRID_ID' => $list_id,
        'COLUMNS' => $columns,
        'ROWS' => $list,
        'SHOW_ROW_CHECKBOXES' => false,
        'NAV_OBJECT' => $nav,
        'AJAX_MODE' => 'Y',
        'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
        'PAGE_SIZES' =>  [
            ['NAME' => '20', 'VALUE' => '20'],
            ['NAME' => '50', 'VALUE' => '50'],
            ['NAME' => '100', 'VALUE' => '100']
        ],
        'AJAX_OPTION_JUMP'          => 'N',
        'SHOW_CHECK_ALL_CHECKBOXES' => false,
        'SHOW_ROW_ACTIONS_MENU'     => true,
        'SHOW_GRID_SETTINGS_MENU'   => true,
        'SHOW_NAVIGATION_PANEL'     => true,
        'SHOW_PAGINATION'           => true,
        'SHOW_SELECTED_COUNTER'     => true,
        'SHOW_TOTAL_COUNTER'        => true,
        'SHOW_PAGESIZE'             => true,
        'SHOW_ACTION_PANEL'         => true,
        'ALLOW_COLUMNS_SORT'        => true,
        'ALLOW_COLUMNS_RESIZE'      => true,
        'ALLOW_HORIZONTAL_SCROLL'   => true,
        'ALLOW_SORT'                => true,
        'ALLOW_PIN_HEADER'          => true,
        'AJAX_OPTION_HISTORY'       => 'N'
    ]);
} else { ?>
    <h2>У данного пользователя нету адресов</h2>
<? } ?>
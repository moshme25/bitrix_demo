<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Data\Cache,
    Bitrix\Main\Loader;

Loader::includeModule('highloadblock');

class CAdressUserList extends CBitrixComponent
{
    /**
     * executes component
     *
     * @return void
     */
    public function executeComponent()
    {
        if ($this->StartResultCache()) {
            $this->arResult = $this->makeResult($this->arParams['SHOW_ONLY_ACTIVE']);
            $this->IncludeComponentTemplate($this->GetTemplateName());
        }
    }

    /**
     * makes arResult
     *
     * @return array
     */
    public function makeResult($showActive = true): array
    {
        $result = [];

        $cache = Cache::createInstance(); 
        if ($cache->initCache(7200, "hl_adress_array1")) { 
            $arItems = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $arFilter = [
                'UF_USER_ID' => \Bitrix\Main\Engine\CurrentUser::get()->getId()
            ];

            if ($showActive == "Y") {
                $arFilter[] = [
                    'UF_ACTIVE' => 1,
                ];
            }
            $arItems = $this->getDataFromHLBlock('UserAdress',$arFilter);
            $cache->endDataCache($arItems); 
        }
        if ($arItems) {
            $result  = [
                'SHOW_TABLE' => true,
                'ITEMS' => $arItems
            ];
        } else {
            $result  = [
                'SHOW_TABLE' => false,
            ];
        }
        return $result;
    }

    /**
     * gets Data From HLBlock
     *
     * @param  string $iblockCode
     * @return array
     */
    public function getDataFromHLBlock($hbCode = '', $arFilter = []): array 
    {
        $result = [];

        if (!$hbCode) {
            return $result;
        }

        $hlblock = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hbCode)->getDataClass();
        $rsData = $hlblock::getList(
            [
                'select' => ['*'],
                'filter' => $arFilter
            ]
        );

        while ($el = $rsData->fetch()) {
            $result[] = $el;
        }
        return $result;
    }
}


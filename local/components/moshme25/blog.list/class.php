<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Application;


class BlogList extends CBitrixComponent
{
    /**
     * Подключение компонента
     *
     * @return void
     */
    public function executeComponent() {
        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();
        if ($this->StartResultCache()) {
            $this->arResult = $this->makeArrayResult($request);
            $this->IncludeComponentTemplate($this->GetTemplateName());
        }
    }

    /**
     * Создание arResult
     * 
     * @param mixed $request
     * 
     * @return array
     */
    public function makeArrayResult(&$request): array
    {
        $result = [];
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $arBlog = [];
            $rsElement = \Bitrix\Iblock\Elements\ElementBlogTable::getList([
                'select' => [
                    'ID',
                    'NAME',
                    'PREVIEW_PICTURE',
                    'PREVIEW_TEXT',
                    'AUTHOR_ID_' => 'AUTHOR_ID',
                    'DETAIL_PAGE_URL_RAW' => 'IBLOCK.DETAIL_PAGE_URL'
                    // 'PICTURES_LIST.FILE'
                ],
                'filter' => ['=ACTIVE' => 'Y'],
                'cache' => [
                    'ttl' => 7200
                ]
            ])->fetchCollection();

            foreach ($rsElement as $element) {
                $arAuthor = $this->getAuthorArray($element->getAuthorId()->getValue());
                $detailUrl = $this->getDetailUrl($element->getId()) ?? '';
                $arBlog[] = [
                    'ID' => $element->getId(),
                    'NAME' => $element->getName(),
                    'PREVIEW_TEXT' => $element->getPreviewText(),
                    'PREVIEW_PICTURE' => CFile::GetPath($element->getPreviewPicture()),
                    'AUTHOR' => $arAuthor,
                    'DETAIL_PAGE_URL' => $detailUrl
                ];
            }

            $result['ITEMS'] = $arBlog;
    
        }
        return $result;
    }

    /**
     * Получение данных об авторе
     * 
     * @param int $authorId
     * 
     * @return array
     */
    public function getAuthorArray($authorId): array
    {
        $res = [];
        if ($authorId) {
            $dbUser = \Bitrix\Main\UserTable::getList([
                'select' => [
                    'ID',
                    'NAME',
                    'LAST_NAME',
                    'PERSONAL_PHOTO'
                ],
                'filter' => [
                    'ID' => $authorId
                ]
            ]);
            if ($arUser = $dbUser->fetch()) {
                $arUser['PERSONAL_PHOTO'] = CFile::GetPath($arUser['PERSONAL_PHOTO']);
                $res = $arUser;
            }
        }
        return $res;
    }
    /**
     * Возращает url на детальную страницу
     * 
     * @param int $id
     * 
     * @return string
     */
    public function getDetailUrl($id): string
    {
        if (!$id) {
            return false;
        }
        $element = \Bitrix\Iblock\Elements\ElementBlogTable::getByPrimary($id, [
            'select' => [
                'ID',
                'DETAIL_PAGE_URL_RAW' => "IBLOCK.DETAIL_PAGE_URL"
            ],
        ])->fetch();
        $url = \CIBlock::ReplaceDetailUrl($element["DETAIL_PAGE_URL_RAW"], $element, true, "E");
        return $url;
    }
}
<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="row news-list bx-site">
	<div class="col">
		<div class="row">
            <?php foreach ($arResult['ITEMS'] as $arItem) { ?>
                <div class="news-list-item mb-2 col-sm">
                    <div class="card">
                        <div class="lesson-info-left-side">
                            <div class="lesson-info-author-and-image">
                                <div class="lesson-info-author">
                                    <div class="lesson-info-img-container">
                                        <div class="crop-container">
                                            <img class="lesson-info-image" src="<?= $arItem['AUTHOR']['PERSONAL_PHOTO'] ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="lesson-info-teacher-name-container">
                                    <div class="lesson-info-teacher-name">
                                        <b><?= $arItem['AUTHOR']['NAME'] . ' ' . $arItem['AUTHOR']['LAST_NAME'] ?></b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                            <img class="card-img-top" src="<?= $arItem['PREVIEW_PICTURE'] ?>" alt="<?= $arItem['NAME'] ?>" title="<?= $arItem['NAME'] ?>">
                        </a>
                        <div class="card-body">
                        <h4 class="card-title">
                            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
                        </h4>
                        <p class="card-text"><?= $arItem['PREVIEW_TEXT'] ?></p>
                        <div class="d-flex justify-content-between">
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="news-list-more">
                                <a class="btn btn-primary btn-sm" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div data-v-656ca0b3="" data-column="1" class="masonry-bottom"></div>
        </div>
    </div>
</div>

<style>
    .lesson-info-image {
	height: 100%;
    width: auto;
    }
    .crop-container {
        width: 50px;
        height: 50px;
        border-radius: 100%;
        overflow: hidden;
    }
</style>
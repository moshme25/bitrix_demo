<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Application;


class BlogDetail extends CBitrixComponent
{
    /**
     * Подключение компонента
     *
     * @return void
     */
    public function executeComponent() {
        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();
        if ($this->StartResultCache()) {
            $this->arResult = $this->makeArrayResult($request);
            $this->IncludeComponentTemplate($this->GetTemplateName());
        }
    }

    /**
     * Создание arResult
     * 
     * @param mixed $request
     * 
     * @return array
     */
    public function makeArrayResult(&$request): array
    {
        $result = [];
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $arBlog = [];
            $arElement = \Bitrix\Iblock\Elements\ElementBlogTable::getByPrimary($this->arParams['ELEMENT_ID'],[
                'select' => [
                    'ID',
                    'NAME',
                    'PREVIEW_PICTURE',
                    'PREVIEW_TEXT',
                    'DETAIL_TEXT',
                    'AUTHOR_ID_' => 'AUTHOR_ID',
                    'PICTURES_LIST.FILE'
                ],
                'filter' => ['=ACTIVE' => 'Y'],
                'cache' => [
                    'ttl' => 7200
                ]
            ])->fetchObject();

            $arAuthor = $this->getAuthorArray($arElement->getAuthorId()->getValue());
            $detailUrl = $this->getDetailUrl($arElement->getId()) ?? '';
            $arPicture = [];
            foreach ($arElement->getPicturesList()->getAll() as $picture) {
                $arPicture[] = '/upload/' . $picture->getFile()->getSubdir() . '/' . $picture->getFile()->getFileName();
            }

            $detailText = $arElement->getDetailText();

            $arSearch = [];
            preg_match_all('/%s\d%/', $detailText, $matches);
            $detailText = str_replace($matches[0], $arPicture, $detailText);

            $arBlog = [
                'ID' => $arElement->getId(),
                'NAME' => $arElement->getName(),
                'PREVIEW_TEXT' => $arElement->getPreviewText(),
                'PREVIEW_PICTURE' => CFile::GetPath($arElement->getPreviewPicture()),
                'AUTHOR' => $arAuthor,
                'DETAIL_PAGE_URL' => $detailUrl,
                'DETAIL_TEXT' => $detailText
            ];

            $result = $arBlog;
    
        }
        return $result;
    }

    /**
     * Получение данных об авторе
     * 
     * @param int $authorId
     * 
     * @return array
     */
    public function getAuthorArray($authorId): array
    {
        $res = [];
        if ($authorId) {
            $dbUser = \Bitrix\Main\UserTable::getList([
                'select' => [
                    'ID',
                    'NAME',
                    'LAST_NAME',
                    'PERSONAL_PHOTO'
                ],
                'filter' => [
                    'ID' => $authorId
                ]
            ]);
            if ($arUser = $dbUser->fetch()) {
                $arUser['PERSONAL_PHOTO'] = CFile::GetPath($arUser['PERSONAL_PHOTO']);
                $res = $arUser;
            }
        }
        return $res;
    }
    /**
     * Возращает url на детальную страницу
     * 
     * @param int $id
     * 
     * @return string
     */
    public function getDetailUrl($id): string
    {
        if (!$id) {
            return false;
        }
        $arElement = \Bitrix\Iblock\Elements\ElementBlogTable::getByPrimary($id, [
            'select' => [
                'ID',
                'DETAIL_PAGE_URL_RAW' => "IBLOCK.DETAIL_PAGE_URL"
            ],
        ])->fetch();
        $url = \CIBlock::ReplaceDetailUrl($arElement["DETAIL_PAGE_URL_RAW"], $arElement, true, "E");
        return $url;
    }
}
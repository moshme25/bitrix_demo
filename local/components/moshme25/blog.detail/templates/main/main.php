<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

?>
<div class="news-detail bx-site">
    <div class="mb-3" id="bx_1878455859_3">
        <div class="lesson-info-left-side">
            <div class="lesson-info-author-and-image">
                <div class="lesson-info-author">
                    <div class="lesson-info-img-container">
                        <div class="crop-container">
                            <img class="lesson-info-image" src="<?= $arResult['AUTHOR']['PERSONAL_PHOTO'] ?>">
                        </div>
                    </div>
                </div>
                <div class="lesson-info-teacher-name-container">
                    <div class="lesson-info-teacher-name">
                        <b><?= $arResult['AUTHOR']['NAME'] . ' ' . $arResult['AUTHOR']['LAST_NAME'] ?></b>
                    </div>
                </div>
            </div>
        </div>
        <h1 class="bl-feed__title mb-05"><?= $arResult['NAME'] ?></h1>
        <div class="news-detail-content">
                <?= $arResult['PREVIEW_TEXT'] ?>
            </div>
        <div class="mb-5 news-detail-img">
            <img class="card-img-top" src="<?= $arResult['PREVIEW_PICTURE'] ?>" alt="<?= $arResult['NAME'] ?>"
                title="<?= $arResult['NAME'] ?>">
        </div>

        <div class="news-detail-body">
            <div class="news-detail-content">
                <?= $arResult['DETAIL_TEXT'] ?>
            </div>
        </div>
    </div>
</div>

<style>
    .lesson-info-image {
	height: 100%;
    width: auto;
    }
    .crop-container {
        width: 100px;
        height: 100px;
        border-radius: 100%;
        overflow: hidden;
    }
</style>
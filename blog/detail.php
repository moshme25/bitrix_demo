<?php
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php';
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Context;

$context = Context::getCurrent();
$request = Context::getCurrent()->getRequest();

?>
<main class="page__main">
    <div class="auth">
        <div class="vacancies__wrapper auth__content is-active">
            <a class="vacancy__breadcrumbs" href="/blog/">Вернуться к списку</a> 
            <?$APPLICATION->IncludeComponent(
                "moshme25:blog.detail",
                "main",
                [
                    'ELEMENT_ID' => $request->get('ID')
                ],
                false
            );?>
        </div>
    </div>
</main>
<?php require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'; ?>
